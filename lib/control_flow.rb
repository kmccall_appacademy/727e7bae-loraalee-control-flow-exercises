# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  lower_case_ch = "abcdefghijklmnopqrstuvwxyz"
  res = ""

  str.each_char do |ch|
    unless lower_case_ch.include?(ch)
      res += ch
    end
  end

  res
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length % 2 == 0
    index_two = str.length / 2
    index_one = index_two - 1

    str[index_one] + str[index_two]
  else
    index = str.length / 2

    str[index]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0

  str.each_char do |ch|
    count += 1 if VOWELS.include?(ch)
  end

  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  res = 1

  (1..num).each do |int|
    res *= int
  end

  res
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  res = ""

  arr.each_with_index do |ele, index|
    res += ele.to_s
    res += separator unless index == arr.length - 1
  end

  res
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  res = ""

  str.each_char.with_index do |ch, index|
    if index % 2 == 0
      res += ch.downcase
    else
      res += ch.upcase
    end
  end

  res
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  string_arr = str.split(" ")

  string_arr.each do |word|
    if word.length >= 5
      word.reverse!
    end
  end

  string_arr.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  res = []

  (1..n).each do |num|
    if num % 15 == 0
      res << "fizzbuzz"
    elsif num % 3 == 0
      res << "fizz"
    elsif num % 5 == 0
      res << "buzz"
    else
      res << num
    end
  end

  res
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  res = []

  arr.each do |ele|
    res.unshift(ele)
  end

  res
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num > 1
    (2...num).each do |int|
      if num % int == 0
        return false
      end
    end
    true
  else
    false
  end
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  res = []

  (1..num).each do |int|
    res << int if num % int == 0
  end

  res
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  num_factors = factors(num)
  res = []

  num_factors.each do |factor|
    if prime?(factor)
      res << factor
    end
  end

  res
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  if odd_array?(arr)
    arr.each do |num|
      return num if num % 2 == 0
    end
  else
    arr.each do |num|
      return num if num % 2 != 0
    end
  end
end

def odd_array?(arr)
  odd_count = 0
  even_count = 0

  arr.each do |num|
    if num % 2 == 0
      even_count += 1
    else
      odd_count += 1
    end
  end

  if odd_count > even_count
    true
  else
    false
  end
end
